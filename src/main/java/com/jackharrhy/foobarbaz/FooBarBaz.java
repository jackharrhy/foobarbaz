package com.jackharrhy.foobarbaz;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

public final class FooBarBaz extends JavaPlugin {
    @Override
    public void onEnable() {
        getLogger().info(getDescription().getName() + " has been enabled");
    }

    @Override
    public void onDisable() {
        getLogger().info(getDescription().getName() + " has been enabled");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        switch (label.toLowerCase()) {
            case "foo":
                sender.sendMessage(ChatColor.DARK_RED + "bar" + ChatColor.BOLD);
                break;
            case "bar":
                sender.sendMessage(ChatColor.DARK_RED + "baz" + ChatColor.BOLD);
                break;
            case "baz":
                sender.sendMessage(ChatColor.DARK_RED + "foo" + ChatColor.BOLD);
                break;
        }
        return true;
    }
}
